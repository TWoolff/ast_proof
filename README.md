## TO DO
01. Page-leave animation on input components when switching from question-page to snack-page
02. Handle multible select boxes on one page
03. Handle text after input
04. Nav progress fill when navigating back
05. Fix skewed chapter images
06. Change hardcoded btnLink in InputSelect.js